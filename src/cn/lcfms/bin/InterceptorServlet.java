/*
 * 控制器拦截器,处理权限问题
 * 作者：成绍勇
 * 2015-11-9
 */
package cn.lcfms.bin;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class InterceptorServlet extends HandlerInterceptorAdapter {

	@Override
	public void afterCompletion(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex)throws Exception {		
		App.printEnd(request);
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		System.out.println(2222);
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	/**
     * 执行模板之前访问
     * @param handler
     * @return
     */
	@Override
	public void postHandle(HttpServletRequest request,HttpServletResponse response, Object handler,ModelAndView modelAndView) throws Exception {
		/*************调用模板之前执行*************/
		this.afterParentAction(handler,request,response,modelAndView);
		super.postHandle(request, response, handler, modelAndView);
	}
	
	/**
	 * 当执行完成action之后将调入的方法
	 * @param handler
	 * @param request
	 * @param response
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	private void afterParentAction(Object handler,HttpServletRequest request,HttpServletResponse response,ModelAndView modelAndView) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method method = null ;
		Object bean =((HandlerMethod)handler).getBean();
		for (Class<?> clazz = bean.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
			try {
				method = clazz.getDeclaredMethod("afterProtected",HttpServletRequest.class,HttpServletResponse.class,ModelAndView.class);
				break;
			} catch (Exception e) {
			}
		}
		method.setAccessible(true);
		method.invoke(bean,request,response,modelAndView);
	}
	/**
	 * 为父类的setProtected属性或方法初始化
	 * @param handler
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	private void beforeParentAction(Object handler,HttpServletRequest request,HttpServletResponse response) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		Method method = null ;	
		Object bean =((HandlerMethod)handler).getBean();
		for (Class<?> clazz = bean.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
			try {
				method = clazz.getDeclaredMethod("beforeProtected",HttpServletRequest.class,HttpServletResponse.class);
				break;
			} catch (Exception e) {				
			}
		}
		method.setAccessible(true);
		method.invoke(bean,request,response);
	}

	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {	
		/*************调用控制器之前执行*************/
		boolean debug=(boolean) App.APPCONFIG.get("debug");
		if(debug){
			DebugUserInfo(request,response);
			beforeParentAction(handler,request,response);
			return super.preHandle(request, response, handler);
		}
		Permission permission;
		//初始化权限
    	if(Permission.PERMIT==null){
    		Permission.PERMIT=new ArrayList<HashMap<String, Object>>();
        	permission = Permission.getpermission();
        	permission.initialize();
        	permission.set_permission(); 	
    	}     
    	String[] mca = (String[]) Userinfo.getUserInfo("mca", request);
	    String m=mca[0];
	    String c=mca[1];
	    String a=mca[2];
		permission = Permission.getpermission();
    	InterceptorServlet.getUserInfo(request, response);
		try {
			//0为未设置权限，1为类设置了权限，2为方法设置了权限，3为all开放
			int if_permit=0;	    
		    Iterator<HashMap<String, Object>> iterator=Permission.PERMIT.iterator();
		    while (iterator.hasNext()) {
		    	HashMap<String, Object> map=iterator.next();
				String action=(String)map.get("action");
				String forward=(String)map.get("forward");
				if(action.equals("cn.lcfms.app."+m+".controller."+c+"."+a)){
					String type=(String)map.get("type");
					if(!type.equals("all")){
						if_permit=2;
						Userinfo.putUserInfo("per_forward", forward, request);
						Userinfo.putUserInfo("perActionID",map.get("id"), request);
						break;
					}else{
						if_permit=3;
					}		
				}
			}
		    iterator=Permission.PERMIT.iterator();
		    if(if_permit==0){
		    	while (iterator.hasNext()) {
			    	HashMap<String, Object> map=iterator.next();
					String action=(String)map.get("action");
					String forward=(String)map.get("forward");
					if(action.equals("cn.lcfms.app."+m+".controller."+c)){
						if_permit=1;
						Userinfo.putUserInfo("per_forward", forward, request);
						Userinfo.putUserInfo("perActionID",map.get("id"), request);
						break;
					}
				}
		    }
		    //该控制器设置有权限，查看权限后访问
		    if(if_permit==1 || if_permit==2){
		    	//获取用户信息
		    	boolean permit = InterceptorServlet.do_permission(request, response,permission);			
				if(permit){// 用户有权限
					System.out.println("====================通过拦截====================");
					beforeParentAction(handler,request,response);
					return super.preHandle(request, response, handler);
				}else{	
					System.out.println("====================用户被拦截====================");
					App.printEnd(request);
					return false;
				}	
		    //该控制器没设置权限，直接访问
		    }else{
		    	System.out.println("====================没有设置权限，直接进入====================");
		    	beforeParentAction(handler,request,response);
		    	return super.preHandle(request, response, handler);
		    }	
		} catch (Exception e) {
			e.printStackTrace();	
			App.printEnd(request);
			return false;
		}	
	}
	/**
	 * 获得用户sessonId,并将用户信息保存到USERINFO这个常量中
	 * @throws Exception 
	 */
	private static void getUserInfo(HttpServletRequest request,HttpServletResponse response){	
		HttpSession session=request.getSession();
		session.setMaxInactiveInterval(3600);
		Userinfo.putUserInfo("JSESSIONID", session.getId(), request);
		Userinfo.putUserInfo("aid", 0, request);
		Userinfo.putUserInfo("aname", "游客", request);
		Userinfo.putUserInfo("gid", 0, request);
		Enumeration<String> names=session.getAttributeNames();
		while(names.hasMoreElements()){
			String name=names.nextElement();
			Userinfo.putUserInfo(name, session.getAttribute(name), request);			
		}  
		System.out.println("用户:"+Userinfo.getUserInfo("aname", request)+",用户Id:"+Userinfo.getUserInfo("aid", request));
	}	
	/**
	 * debug环境下设置用户信息
	 * @throws Exception 
	 */
	private static void DebugUserInfo(HttpServletRequest request,HttpServletResponse response){	
		int aid=(int) App.APPCONFIG.get("debugAid");
		String aname=(String) App.APPCONFIG.get("debugAname");
		int gid=(int) App.APPCONFIG.get("debugGid");	
		HttpSession session=request.getSession();
		session.setMaxInactiveInterval(3600);
		Userinfo.putUserInfo("JSESSIONID", session.getId(), request);
		Userinfo.putUserInfo("aid", aid, request);
		Userinfo.putUserInfo("aname", aname, request);
		Userinfo.putUserInfo("gid", gid, request);
		Enumeration<String> names=session.getAttributeNames();
		while(names.hasMoreElements()){
			String name=names.nextElement();
			Userinfo.putUserInfo(name, session.getAttribute(name), request);			
		}  
		System.out.println("用户:"+Userinfo.getUserInfo("aname", request)+",用户Id:"+Userinfo.getUserInfo("aid", request));
	}	
	//判断用户权限
	private static boolean do_permission(HttpServletRequest request,HttpServletResponse response,Permission permission) throws IOException{
		boolean if_permit = permission.if_permisson(request);
		if (!if_permit) {
			String forward = (String)Userinfo.getUserInfo("per_forward", request);
			response.sendRedirect(forward);		
			return false;
		}
		return true;
	}	
}


