package cn.lcfms.bin;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DefaultPageFilter implements Filter {
	
	private FilterConfig config;

	@Override
	public void destroy() {
		System.out.println("过滤器销毁");
	}

	@Override
	public void doFilter(ServletRequest r1, ServletResponse r2,FilterChain chain) throws IOException, ServletException {						
		HttpServletRequest request = (HttpServletRequest) r1;
		HttpServletResponse response = (HttpServletResponse) r2;
		response.setContentType("text/html;charset=utf-8");
		Userinfo.putUserInfo("TIMESTART", new Date().getTime(), request);
		Userinfo.putUserInfo("MEMORYSTART", Runtime.getRuntime().freeMemory(), request);
		String uri=request.getRequestURI();
		String contextPath = request.getContextPath();
		String path=uri.substring(contextPath.length());
		if(!path.equals("/") && path.endsWith("/")){
			response.sendRedirect(uri.substring(0, uri.length()-1));			
			return;
		}
		if(path.indexOf(".html") !=-1 || path.indexOf(".ico") !=-1 || path.startsWith("/statics")){
			chain.doFilter(request,response);	
			return;
		}
		System.out.println("进入路由解析器");
		System.out.println(path);
		Enumeration<String> initParameterNames = config.getInitParameterNames();
		while(initParameterNames.hasMoreElements()){
			String name=initParameterNames.nextElement();
			if(path.equals(name)){
				String url=config.getInitParameter(name);
				RequestDispatcher dispatcher = request.getRequestDispatcher(url);
				setMca(url,request);
				dispatcher.forward(request, response);
				return;
			}
		}	
		setMca(path,request);	
		chain.doFilter(request,response);		
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config=config;		
	}
	
	private static void setMca(String uri,HttpServletRequest request){
		String[] split = uri.substring(1).split("/");
		if(split.length<3){
			 System.out.println("请求的路由不存在");
			 return;
		}
		split[1]=split[1].replaceFirst(split[1].substring(0, 1),split[1].substring(0, 1).toUpperCase());		
		Userinfo.putUserInfo("mca", split, request);
		System.out.println("===============模块名："+split[0]+"===============控制器："+split[1]+"===============方法名："+split[2]+"===============");
	}

}
