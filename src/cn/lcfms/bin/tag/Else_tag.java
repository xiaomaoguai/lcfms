package cn.lcfms.bin.tag;

import javax.servlet.jsp.tagext.BodyTagSupport;

public class Else_tag extends BodyTagSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * @return int
     */
    @Override
    public int doStartTag() {
    	If_tag ifTag = If_tag.getIfTag(this.pageContext);
        if(ifTag == null) {
            throw new RuntimeException("IfTag not match !");
        }

        If_tag.remove(this.pageContext);
        if(ifTag.complete()) {
            return SKIP_BODY;
        }
        else {
            ifTag.finish();
            return EVAL_BODY_INCLUDE;
        }
    }
}
