package cn.lcfms.bin.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import cn.lcfms.bin.Userinfo;

public class Permission_tag extends TagSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String gid;

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public int doStartTag() throws JspException {
		try {
			if (this.gid == null || this.gid.equals("")) {
			   return EVAL_BODY_INCLUDE;
			}
			// 获取用户组的标签权限
			String[] ids=this.gid.split(",");			
			HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();	
			int groupId=(int) Userinfo.getUserInfo("gid", request);
			for(String id:ids){
				int i=Integer.valueOf(id);
				if(i==groupId){
					return EVAL_BODY_INCLUDE;
				}										
			}					
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

	public int doEndTag() throws JspTagException {
		return EVAL_PAGE;
	}

	private String test;

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
}
