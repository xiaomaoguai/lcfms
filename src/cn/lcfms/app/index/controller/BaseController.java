package cn.lcfms.app.index.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

public class BaseController {
	/**
	 * 该方法将在执行控制器之前被执行
	 */
    protected void beforeProtected(HttpServletRequest request,HttpServletResponse response){
    	String contextPath = request.getContextPath();
	    String basePath = "";
	    if(request.getServerPort()==80){
	    	basePath = request.getScheme()+"://"+request.getServerName()+contextPath+"/";
	    }else{
	    	basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
	    }	
	    request.setAttribute("APP", basePath); 
    	request.setAttribute("JS", basePath+"statics/ace/js/"); 
    	request.setAttribute("CSS", basePath+"statics/ace/css/"); 
    	request.setAttribute("IMG", basePath+"statics/ace/images/"); 
    }  
    /**
   	 * 该方法将在执行控制器之后，调入模板之前被执行
   	 */
    protected void afterProtected(HttpServletRequest request,HttpServletResponse response,ModelAndView view){
    	
    }
}
