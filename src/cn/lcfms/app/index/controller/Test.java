package cn.lcfms.app.index.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.lcfms.app.index.service.TestService;
import cn.lcfms.app.index.service.dao.TestDao;
import cn.lcfms.bean.Demo;
import cn.lcfms.bin.PermitPoll;
import cn.lcfms.service.DemoService;
import cn.lcfms.service.dao.DemoDao;
import cn.lcfms.unit.Vardump;
@Controller("index.Test")
@RequestMapping("/index/test")
@PermitPoll(type="测试",name="测试service",forward="login.html")
public class Test extends BaseController{

	@Autowired
	private DemoDao dao1;
	
	@Autowired
	private TestDao dao2;
	
	@Autowired
	private DemoService service1;
	
	@Autowired
	private TestService service2;
	
	@ResponseBody	
	@RequestMapping("/test1")
	public String test1(){
		Demo demo = dao1.getDemo(1);
		Vardump.print(demo);
		return "测试外层dao";
	}
	
	@ResponseBody	
	@RequestMapping("/test2")
	public String test2(){
		List<?> list = dao2.getList();
		Vardump.print(list);
		return "测试内层dao";
	}
			
	@ResponseBody	
	@RequestMapping("/test3")
	public String test3(){
		service1.getDemo();
		return "测试外层service";
	}
			
	@ResponseBody	
	@RequestMapping("/test4")
	public String test4(){
		service2.getList();
		return "测试内层service";
	}
}
