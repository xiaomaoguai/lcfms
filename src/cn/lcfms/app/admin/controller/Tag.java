package cn.lcfms.app.admin.controller;


import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.lcfms.app.admin.bean.TagBean;
import cn.lcfms.bin.App;
import cn.lcfms.bin.PermitPoll;
import cn.lcfms.service.BaseService;
import cn.lcfms.unit.Vardump;


@PermitPoll(type="系统管理",name="标签管理",forward="login.html")
@Controller("admin.Tag")
@RequestMapping("/admin/tag")
public class Tag extends BaseController{
	/*
	 * 管理标签
	 */
	@RequestMapping("/manage")
	public ModelAndView manage(){
		BaseService service=App.getService("tag");
		List<HashMap<String, Object>> list = service.selectList();
		ModelAndView view=new ModelAndView("admin/frame/tagManage","list",list);
		return view;
	}
	/*
	 * 增加标签页面
	 */
	@RequestMapping("/addview")
	public ModelAndView addview(TagBean bean){
		BaseService service=App.getService("tag");
		ModelAndView view=new ModelAndView("admin/frame/tagAdd");
		List<HashMap<String, Object>> list = service.where("parentId=0").selectList();
		view.addObject("tagType", list);
		if(bean.getTagId()!=0){
			bean=service.selectOne(TagBean.class, bean.getTagId());
		}
		Vardump.print(bean);
		view.addObject("tag", bean);
		return view;
	}
	/*
	 * 处理增加或修改标签
	 */
	@RequestMapping("/add")
	public String add(TagBean bean){
		BaseService service=App.getService("tag");
		if(bean.getTagId()==0){
			service.insert(bean);
		}else{
			service.update(bean);
		}	
		return "local:manage";
	}
	/*
	 * 删除标签
	 */
	@RequestMapping("/delete")
	public String delete(int tagId){
		BaseService service=App.getService("tag");
		service.deleteById(tagId);
		return "local:manage";
	}
}
