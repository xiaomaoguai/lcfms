window.form=(function($){
	var form={};	
	form.uploadfile=[];
	function showSuccess(formname,name,message){
		var b=$(document[formname][name]);
		b.parents(".form-group").removeClass("has-error");
		b.parents(".form-group").addClass("has-success");
		b.nextAll().addClass("hide");
		b.nextAll(".success").removeClass("hide");
		if(message){
			b.nextAll(".success").html("<i class=\"glyphicon glyphicon-ok\"></i> "+message)
		}
	}
	function showError(formname,name,message){
		var b=$(document[formname][name]);
		b.parents(".form-group").removeClass("has-success");
		b.parents(".form-group").addClass("has-error");
		b.nextAll().addClass("hide");
		b.nextAll(".error").removeClass("hide");
		if(message){
			b.nextAll(".error").html("<i class=\"glyphicon glyphicon-remove\"></i> "+message)
		}
	}	
	function checkForm(val,attribute){
			if(!attribute){
				return false;
			}
			if(attribute.indexOf("unempty")!=-1 && val==""){
				return "输入内容不能为空！";
			}
			if(attribute.indexOf(" gt")!=-1){
				var gt=attribute.split(" ");
				var t=0;
				for(var l in gt){					
					var p=parseInt(gt[l].substr(2));				
					if(t<p)t=p;
				}
				if(val.length<t){
					return "输入内容长度必须大于"+t+"！";
				}
			}
			if(attribute.indexOf(" lt")!=-1){
				var lt=attribute.split(" ");
				var t=99999;
				for(var l in lt){					
					var p=parseInt(lt[l].substr(2));				
					if(t>p)t=p;
				}
				if(val.length>t){
					return "输入内容长度必须小于"+t+"！";
				}
			}
			if(attribute.indexOf("number")!=-1 && isNaN(val)){
				return "输入内容必须为数字！";
			}
			if(attribute.indexOf("integer")!=-1){
				var reg1 =/^[-+]?\d*$/;
				if(val.trim().match(reg1) == null){
					return "输入内容必须为整数！";
				}		
			}
			if(attribute.indexOf("+integer")!=-1){
				var reg1 =  /^\d+$/;
				if(val.trim().match(reg1) == null)
				return "输入内容必须为大于0的整数！";
			}
			if(attribute.indexOf("unchinaChar")!=-1){
				var reg1 = /[A-Za-z\d_]+/;  			
				if(val.trim().match(reg1) == null)
				return "输入内容必须为中文！";
			}
			if(attribute.indexOf("unNum")!=-1 && !isNaN(val)){
				return "输入内容不能为数字！";
			}
			if(attribute.indexOf("email")!=-1){	
				var szReg=/^[A-Za-zd]+([-_.][A-Za-zd]+)*@([A-Za-zd]+[-.])+[A-Za-zd]{2,5}$/; 
				var c=szReg.test(val); 
				if(!c)
				return "输入内容不是邮件！";
			}
			if(attribute.indexOf("mobile")!=-1){
				var reg = /^1[3|4|5|7|8][0-9]{9}$/; 
				var flag = reg.test(val);
				if(!flag)
				return "输入内容不是手机号！";
			}
			return false;
	}
	function showFocus(formname,name){
		$(document[formname][name]).focus();
	}
	function dropzone(element){	
		var maxFilesize=$(element).attr("maxFilesize");
		var url=$(element).attr("url");
		var maxFiles=$(element).attr("maxFiles");
		var acceptedFiles=$(element).attr("acceptedFiles");	
		$(element).dropzone({								    
			paramName: "file",								    
			maxFilesize: maxFilesize,		
			url:url,																		
			addRemoveLinks : true,									
			maxFiles:maxFiles,									
			acceptedFiles:acceptedFiles,											
			dictResponseError: '上传失败!',									
			init: function(){
				this.on("addedfile", function(file){
					
				});
				this.on("removedfile", function(file){
					for(var i=0; i<form.uploadfile.length; i++) {
					    if(form.uploadfile[i] == file) {
					       form.uploadfile.splice(i, 1);
					       break;
					    }
					}	
				});
				this.on("success",function(file, responseText){
					if(!responseText.filePath){
						alert("一个文件上传成功后请返回一个json,必须包含一个名filePath的属性，以保存图片的路径，方便二次提交！");
						return;
					}else{
						file.value=responseText;
						form.uploadfile.push(file);
					}					
				});
			}								 
		});		
	}
	var SelectedForm=function(name){
		this.name=name;
		this.submit;
		this.error;
		this.success;
		this.focus;
	}
	form.init=function(formname){	
		$(".form-group .success").remove();
		$(".form-group .error").remove();
		$(".form-group").removeClass("has-success");
		$(".form-group").removeClass("has-error");	
		var o=new SelectedForm(formname);	
		$("form[name='"+formname+"'] .form-control").each(function(){			
			var success=$(this).attr("success");
			var error=$(this).attr("error");
			var attribute=$(this).attr("attribute");
			var name=$(this).attr("name");				
			if(success){
				var str="<span class=\"help-block success hide\"><i class=\"glyphicon glyphicon-ok\"></i> "+success+"</span>";
				$(this).after(str);
			}
			if(error){
				var str="<span class=\"help-block error hide\"><i class=\"glyphicon glyphicon-remove\"></i> "+error+"</span>";
				$(this).after(str);
			}
			if(this.tagName=="INPUT" && this.type=="text"){
				$(this).blur(function(){	
					var val=$(this).val();
					var r=checkForm(val,attribute);
					if(r!==false){
						showError(formname,name,r);
					}else{
						showSuccess(formname,name);
					}
				});	
			}
			if(this.tagName=="INPUT" && this.type=="password"){
				$(this).blur(function(){	
					var val=$(this).val();
					var r=checkForm(val,attribute);
					if(r!==false){
						showError(formname,name,r);
					}else{
						showSuccess(formname,name);
					}
				});	
			}
            if(this.tagName=="TEXTAREA"){
				    var maxnum=$(this).attr("maxnum");
					if(maxnum!=0){
						var str="<span class=\"help-block success hide\"></span>";
						$(this).after(str);
						var str="<span class=\"help-block error hide\"></span>";
						$(this).after(str);	
						$(this).keyup(function(){						
							var val=$(this).val();
							if(val.length>maxnum){
								showError(formname,name,"超出了"+(val.length-maxnum)+"个字");
							}else{
								showSuccess(formname,name,"还能输入"+(maxnum-val.length)+"个字");
							}
						}); 						
					}               									
			}
			if(this.tagName=="INPUT" && this.type=="radio"){
											
			}
			if(this.tagName=="INPUT" && this.type=="checkbox"){
				   									
			}	
			if(this.tagName=="INPUT" && $(this).hasClass('calendar')){
				var k=document.getElementsByClassName('calendar');
					for(var i1=0;i1<k.length;i1++){
						var months=$(k[i1]).attr('months');
						var mode=$(k[i1]).attr('mode');
						var direction=$(k[i1]).attr('direction');
						new Kalendae.Input(k[i1], {
							months:months,
							mode:mode,
							direction:direction,
							selected:Kalendae.moment().subtract({M:0})
					});		
				}									
			}		
		});	
		
		$(".dropzone").each(function(){
			dropzone(this);
		});
		
		return o;
	}
	//提交
	SelectedForm.prototype.submit=function(){	
		var formname=this.name;
		var f=true;
		$("form[name='"+formname+"'] .form-control").each(function(){
			var attribute=$(this).attr("attribute");
			var val=$(this).val();
			var name=$(this).attr("name");	
			var r=checkForm(val,attribute);
			if(r!==false){
				showError(formname,name,r);	
				showFocus(formname,name);
				f=false;
			}
		});		
		if(f){
			if(form.uploadfile.length>0){
				for(var i=0;i<form.uploadfile.length;i++){
					var input=document.createElement("input");
					input.name="imgFile";
					input.type="hidden";
					input.value=form.uploadfile[i].value.filePath;
					$(document[this.name]).append(input);
				}			
			}
			document[this.name].submit();
		}	
	}
	//报错
	SelectedForm.prototype.error=function(name,message){
		showError(this.name,name,message);
	}
	//重置
	SelectedForm.prototype.reset=function(){
		form.uploadfile=[];
		form.init(this.name);
	}
	//提示成功
	SelectedForm.prototype.success=function(name,message){
		showSuccess(this.name,name,message);
	}
	//获取焦点
	SelectedForm.prototype.focus=function(name){
		showFocus(this.name,name);
	}
	//获取值
	SelectedForm.prototype.getValue=function(name){
		return document[this.name][name].value;
	}
	//判断有无错误
	SelectedForm.prototype.hasError=function(){
		var b=$(document[this.name]).children().find(".has-error");
		if(b.length>0){
			return true;
		}
		return false;
	}
	
	return form;
})($)
