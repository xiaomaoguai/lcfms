/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : lcfms

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-09-11 21:27:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for laocheng_process_active
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process_active`;
CREATE TABLE `laocheng_process_active` (
  `mid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`mid`,`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process_active
-- ----------------------------
INSERT INTO `laocheng_process_active` VALUES ('73', '2', '1');
INSERT INTO `laocheng_process_active` VALUES ('74', '2', '1');
INSERT INTO `laocheng_process_active` VALUES ('74', '4', '0');

-- ----------------------------
-- Table structure for laocheng_process_form
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process_form`;
CREATE TABLE `laocheng_process_form` (
  `sid` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process_form
-- ----------------------------
INSERT INTO `laocheng_process_form` VALUES ('69', 'timeslice', '使用日期', '2017-09-01&douhao;2017-09-26');
INSERT INTO `laocheng_process_form` VALUES ('69', 'text', '使用原因', '的撒撒打算的');
INSERT INTO `laocheng_process_form` VALUES ('69', 'date', '归还日期', '2017-09-29');
INSERT INTO `laocheng_process_form` VALUES ('72', 'select', '是否同意', '拒绝通过');
INSERT INTO `laocheng_process_form` VALUES ('72', 'text', '审核备注', '佛挡杀佛第三方');
INSERT INTO `laocheng_process_form` VALUES ('73', 'timeslice', '使用日期', '2017-09-01&douhao;2017-09-12');
INSERT INTO `laocheng_process_form` VALUES ('73', 'text', '使用原因', '丢失的大佛');
INSERT INTO `laocheng_process_form` VALUES ('73', 'date', '归还日期', '2017-09-29');
INSERT INTO `laocheng_process_form` VALUES ('74', 'select', '是否同意', '同意通过');
INSERT INTO `laocheng_process_form` VALUES ('74', 'text', '审核备注', '发电示范店');

-- ----------------------------
-- Table structure for laocheng_process_manage
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process_manage`;
CREATE TABLE `laocheng_process_manage` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `oname` varchar(50) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `uname` varchar(50) DEFAULT NULL,
  `applyTime` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `end` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process_manage
-- ----------------------------
INSERT INTO `laocheng_process_manage` VALUES ('73', '1', '1', '市场部', '23', '管理员', '2017-09-11 17:27:40', '1', '失败');
INSERT INTO `laocheng_process_manage` VALUES ('74', '1', '1', '市场部', '23', '管理员', '2017-09-11 17:55:51', '0', null);

-- ----------------------------
-- Table structure for laocheng_process_step
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process_step`;
CREATE TABLE `laocheng_process_step` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `mid` int(11) DEFAULT NULL,
  `stepId` int(11) DEFAULT NULL,
  `stepName` varchar(50) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `realname` varchar(50) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `oname` varchar(50) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process_step
-- ----------------------------
INSERT INTO `laocheng_process_step` VALUES ('69', '1', '73', '1', '申请用车', '23', '管理员', '1', '市场部', '2017-09-11 17:27:40');
INSERT INTO `laocheng_process_step` VALUES ('72', '1', '73', '2', '资格审核', '25', '绍', '3', '行政部', '2017-09-11 17:49:27');
INSERT INTO `laocheng_process_step` VALUES ('73', '1', '74', '1', '申请用车', '23', '管理员', '1', '市场部', '2017-09-11 17:55:51');
INSERT INTO `laocheng_process_step` VALUES ('74', '1', '74', '2', '资格审核', '25', '绍', '3', '行政部', '2017-09-11 17:56:02');
