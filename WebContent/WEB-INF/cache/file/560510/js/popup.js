window.popup=(function(){
	$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
		_title: function(title) {
			var $title = this.options.title || '&nbsp;';
			if( ("title_html" in this.options) && this.options.title_html == true ){
				title.html($title);
			}else{
				title.text($title);
			} 
		}
	}));
	var obj={};
	obj.open=function(id,ok,cancel){
		var d=$("#dialog-id-"+id);
		var modal=(d.attr("modal")=='true')?true:false;
		var resizable=(d.attr("resizable")=='true')?true:false;
		var draggable=(d.attr("draggable")=='true')?true:false;
		var width=d.attr("width");
		var height=d.attr("height");
		var title=d.attr("toptitle");
		d.removeClass('hide');	
    	d.dialog({
			modal: modal,
			resizable: resizable,
			draggable:draggable,
			width:width,
			height:height,
			title: title,
			title_html: true,
			buttons: [{
					html: "<i class='fa fa-remove bigger-110'></i>&nbsp;&nbsp;取消",
					"class" : "btn btn-danger btn-xs ",
					click: function() {cancel();$( this ).dialog( "close" );} 
					  },
					  {
					html: "<i class='fa fa-check bigger-110'></i>&nbsp;&nbsp;确认",
					"class" : "btn btn-success btn-xs",
					click: function() {ok();$( this ).dialog( "close" ); } 
					}]
	   });
	   d.focus();
	}
	return obj;
})()
