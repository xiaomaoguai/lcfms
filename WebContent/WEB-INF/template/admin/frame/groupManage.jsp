<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>	
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="${APP}statics/tag/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="${APP}statics/tag/css/font-awesome.css"/>
		<link rel="stylesheet" href="${APP}statics/tag/css/widgets.css"/>
		<link rel="stylesheet" href="${APP}statics/tag/css/b.page.bootstrap3.css"/>
		<script src="${APP}statics/tag/js/jquery-3.2.1.min.js"></script>
		<script src="${APP}statics/tag/js/bootstrap.min.js"></script>
		<script src="${APP}statics/tag/js/widgets.js"></script>
		<script type="text/javascript" src="${APP}statics/tag/js/b.page.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="widget-box  widget-color-blue" id="3061">
					<div class="widget-header widget-header-small">
						<h6 class="widget-title">
							<i class="fa fa-sort">
							</i>
							用户组管理
						</h6>
						<div class="widget-toolbar">
							<a href="javascript:void(0);" data-action="fullscreen">
								<i class="fa fa-expand">
								</i>
							</a>
							<a href="javascript:void(0);" data-action="collapse">
								<i class="fa fa-chevron-up">
								</i>
							</a>
							<a href="javascript:void(0);" data-action="close">
								<i class="fa fa-minus">
								</i>
							</a>
						</div>
					</div>
					<div class="widget-body">
						<div class="widget-body-inner" style="display: block;">
							<div class="widget-main"  style="padding: 12px;">
								<table class="bTable table table-striped table-bordered table-hover table-condensed">
								<thead>
									<tr>
										<th align="center" width="70px">
											<label for="checkAll">
												<input type="checkbox" id="checkAll">
												选择
											</label>
										</th>
										<th style="width:45px">组ID</th>
										<th style="width:100px">组名称</th>
										<th style="width:250px">组描述</th>
										<th>控制器权限</th>
										<th style="width:150px">管理操作</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${list}" var="val">
									<tr>
										<td align="center">
											<input type="checkbox" name="boxSelect" value="${val.aid}">
										</td>
									    <td>${val.gid}</td>
										<td>${val.gname}</td>
										<td>${val.gdesc}</td>
										<td>
										<c:forEach items="${val.gpermission}" var="bal">
										${bal}&nbsp;&nbsp;
										</c:forEach>
										</td>
										<td>
										<a href="add_group_view?gid=${val.gid}"><i class="fa fa-edit bigger-120"></i> 修改</a>&nbsp;&nbsp;
	    					   			<a href="delete_group?gid=${val.gid}"><i class="fa fa-trash bigger-120"></i> 删除</a>	
										</td>
									</tr>
								 </c:forEach>							
								</tbody>
							</table>
						</div>
						</div>
					</div>
					<div id="page" pagenumber="1" pagesize="10" totalrow="100"></div>	
				</div>
			</div>
		</div>
		<script>					
		$(function(){
			widget.init();
			//当前页	
			var pageNumber=$('#page').attr("pageNumber");
			//每页多少行	
			var pageSize=$('#page').attr("pageSize");
			//总共多少行
			var totalRow=$('#page').attr("totalRow");
			//总共多少页
			var totalPage=Math.ceil(totalRow/pageSize);
			$('#page').bPage({  
			    url : '',
			    totalPage :totalPage,
			    totalRow :totalRow,
			    pageSize :pageSize,
			    pageNumber:pageNumber
			});	
		});
		</script>
	</body>
</html>