<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>欢迎登录lcfms管理后台</title>
	<link rel="stylesheet" type="text/css" href="${CSS_PATH}default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${CSS_PATH}icon.css">
	<link rel="stylesheet" type="text/css" href="${CSS_PATH}frame.css">
	<script type="text/javascript" src="${JS_PATH}jquery.min.js"></script>
	<script type="text/javascript" src="${JS_PATH}jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${JS_PATH}easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="${JS_PATH}common.js"></script>	
</head>
<body class="easyui-layout" id="layoutset">
	<div data-options="region:'north',height:'auto'">
	<div class="yukaej_top">
     <div class="yukaej_logo"></div>
     <div class="yukaej_right">          
       	<ul>
       	<li><a href="#">欢迎您，${USERINFO.aname}！</a></li>
       	<li><a href="javascript:easyui.showmenu();">帮助</a></li>
		<li><a href="${APP_PATH}system/frame/clearCache">更新缓存</a></li>		
		<li><a href="${APP_PATH}system/user/quit">退出登录</a></li>
		<li><a href="#">修改密码  </a></li>
		</ul>
     </div>
  	</div>
  	<div class="yukaej_bottom">
    <ul class="yukaej_ul">
    <lc:item item="itemMap" parentId="1">   
	<lc:permit gid="${itemMap.permitId}">
	<li id="menu_${itemMap.itemId}" <c:if test="${itemMap.itemId==TOPMENUID}">class="lione"</c:if>><a href="javascript:void(0);" onclick="easyui.change_left_item(${itemMap.itemId},'${itemMap.itemName}','${itemMap.icon}');"><span class="${itemMap.icon}">&nbsp;&nbsp;&nbsp;&nbsp;</span>${itemMap.itemName}</a></li>		
	</lc:permit>
	</lc:item>	
	</ul> 	
  	</div>
	</div>
	<div data-options="region:'south',height:20">
	</div> 
	<div data-options="region:'west',collapsible:true,iconCls:'${TOPMENUICON}',title:'${TOPMENUNAME}'" style="width:200px;" id="systemName" >	
	    <lc:item item="AultItemMap" parentId="1">
		<lc:permit gid="${AultItemMap.permitId}">
		<div class="easyui-accordion itemline" data-options="fit:true,border:false" id="item_${AultItemMap.itemId}" <c:if test="${AultItemMap.itemId!=TOPMENUID}">style="display:none;"</c:if>>	
		     <lc:item item="leftItemMap" parentId="${AultItemMap.itemId}">
			 <lc:permit gid="${leftItemMap.permitId}">
			 <div title="${leftItemMap.itemName}" style="padding:2px 10px;" class="itembox" data-options="iconCls:'${leftItemMap.icon}'">
			      <ul class="easyui-tree" data-options="lines:true">
			      <lc:item item="AleftItemMap" parentId="${leftItemMap.itemId}">
				  <lc:permit gid="${AleftItemMap.permitId}">
				  <li data-options="iconCls:'${AleftItemMap.icon}'"><a href="javascript:void(0);" onclick="javascript:easyui.addTab('${AleftItemMap.itemName}','${APP_PATH}${AleftItemMap.url}');">${AleftItemMap.itemName}</a></li>
				  </lc:permit>
				  </lc:item>
				  </ul>
			 </div>
			 </lc:permit>
			 </lc:item>	
		</div>	
		</lc:permit>
		</lc:item>
	</div>		
	<div data-options="region:'center',border:true">
		<div class="easyui-tabs" id="maintabs" data-options="fit:true,border:false,plain:true">
			<div title="首页" data-options="href:'first'"></div>
		</div>
	</div>
	<div id="rightMm" class="easyui-menu cs-tab-menu">
		<div id="rightMm-tabupdate">刷新</div>
		<div class="menu-sep"></div>
		<div id="rightMm-tabclose">关闭</div>
		<div id="rightMm-tabcloseother">关闭其他</div>
		<div id="rightMm-tabcloseall">关闭全部</div>
		<div id="rightMm-tabcloseright">关闭右边</div>
		<div id="rightMm-tabcloseleft">关闭左边</div>
	</div>
	<div id="help-menu" class="easyui-menu" style="width:120px;">
		<div data-options="name:'浏览icon'" onclick="javascript:easyui.addTab('浏览icon','/admin/index/first');">浏览icon</div>
	</div>
</body>
</html>