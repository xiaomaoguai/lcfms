<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>
<ul class="nav nav-list" id="leftItem">	
<lc:item  parentId="${USERINFO.firstitemid}" item="First_item">  
    <lc:permit gid="${First_item.permitId}">				
	<li id="leftItemId_${First_item.itemId}">
		<a href="javascript:void(0);" <c:if test="${First_item.hasChild == true}">class="dropdown-toggle"</c:if> title="${First_item.itemName}" url="${First_item.url}" itemId="${First_item.itemId}">
			<i class="menu-icon fa ${First_item.icon}"></i>
			<span class="menu-text"> ${First_item.itemName}</span>
			<c:if test="${First_item.hasChild == true}"><b class="arrow fa fa-angle-down"></b></c:if>
		</a>
		<ul class="submenu">
		    <lc:item  parentId="${First_item.itemId}" item="Second_item">
		    <lc:permit gid="${Second_item.permitId}">		   						
			<li id="leftItemId_${Second_item.itemId}">
				<a href="javascript:void(0);"  <c:if test="${Second_item.hasChild == true}">class="dropdown-toggle"</c:if> title="${Second_item.itemName}" url="${Second_item.url}" itemId="${Second_item.itemId}">
					<i class="menu-icon fa ${Second_item.icon}"></i>
					<span class="menu-text"> ${Second_item.itemName}</span>
					<c:if test="${Second_item.hasChild == true}"><b class="arrow fa fa-angle-down"></b></c:if>
				</a>
				<ul class="submenu">
				  <lc:item  parentId="${Second_item.itemId}" item="Third_item">	
				  	   <lc:permit gid="${Third_item.permitId}">						  	   						
					   <li id="leftItemId_${Third_item.itemId}">
						  <a href="javascript:void(0);" <c:if test="${Third_item.hasChild == true}">class="dropdown-toggle"</c:if> title="${Third_item.itemName}" url="${Third_item.url}" itemId="${Third_item.itemId}">
							<i class="menu-icon fa ${Third_item.icon}"></i>
							<span class="menu-text"> ${Third_item.itemName}</span>
							<c:if test="${Third_item.hasChild == true}"><b class="arrow fa fa-angle-down"></b></c:if>
						   </a>
						   <ul class="submenu">
							  <lc:item  parentId="${Third_item.itemId}" item="Fourth_item">					  	   						
								   <lc:permit gid="${Fourth_item.permitId}">
								   <li id="leftItemId_${Fourth_item.itemId}">
									  <a href="javascript:void(0);"  title="${Fourth_item.itemName}" url="${Fourth_item.url}" itemId="${Fourth_item.itemId}">
										<i class="menu-icon fa ${Fourth_item.icon}"></i>
										<span class="menu-text"> ${Fourth_item.itemName}</span>										
									   </a>
								   </li>
								   </lc:permit>
							  </lc:item>
							</ul>
					   </li>
					  </lc:permit>
				  </lc:item>
				</ul>
			</li>
			</lc:permit>
			</lc:item>
		</ul>
	</li>
	</lc:permit> 	
</lc:item>  				 
</ul>