<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}b.page.bootstrap3.css" type="text/css">
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="${JS}b.page.js"></script>
</head>
<body>
<div class="container-fluid">
<div class="row">
	<table class="bTable table table-striped table-bordered table-hover table-condensed">
	    <thead>
	        <tr>
	            <th align="center" width="70px"><label for="checkAll"><input type="checkbox" id="checkAll"/> 选择</label></th>
	            <th><label><i class="fa fa-sort"></i> 测试列1</label></th>
	            <th>测试列2</th>
	            <th>测试列3</th>
	            <th>测试列4</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<c:forEach items="1,2,3" var="id">
	    	<tr>
	        	<td align="center"><input type="checkbox" name="boxSelect" value="${id}"/></td>
	            <td>测试数据</td>
	            <td>测试数据</td>
	            <td>测试数据</td>
	            <td>测试数据</td>  
	        </tr>	   
	        </c:forEach>
	    </tbody>
	</table>
	<div id="page" pageNumber="${pageNumber}" pageSize="10" totalRow="100"></div>
</div>
</div>
<script>
function init(){
	//当前页	
	var pageNumber=$('#page').attr("pageNumber");
	//每页多少行	
	var pageSize=$('#page').attr("pageSize");
	//总共多少行
	var totalRow=$('#page').attr("totalRow");
	//总共多少页
	var totalPage=Math.ceil(totalRow/pageSize);
	$('#page').bPage({  
	    url : 'page',
	    totalPage :totalPage,
	    totalRow :totalRow,
	    pageSize :pageSize,
	    pageNumber:pageNumber
	});	
}
init();
</script>
</body>
</html>
 