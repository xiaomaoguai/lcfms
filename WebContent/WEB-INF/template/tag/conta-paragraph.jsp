<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;height: 400px;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">段落样式：</label>
	<div class="controls">
		<select class="form-control input-medium" name="style">
			<option value="">灰底默认</option>
			<option value="paragraph-danger">红底danger</option>
			<option value="paragraph-warning">黄底warning</option>
			<option value="paragraph-success" selected>绿底success</option>
			<option value="paragraph-info">蓝底info</option>						
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">可关闭：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="close" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="close" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>		
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">首字空格：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="kongge" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="kongge" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>		
	</div>
</div>
<div class="form-group" style="width:90%;float:right;">
	<script type="text/javascript" src="${CSS}../../kindeditor/kindeditor-all-min.js" charset="utf-8"></script>
	<script type="text/javascript" src="${CSS}../../kindeditor/lang/zh-CN.js" charset="utf-8"></script>
	<textarea name="content" style="visibility:hidden;width:100%;min-height:300px;" class="kindeditor"><p>内容</p></textarea>
	<script type="text/javascript">		
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create('textarea[class="kindeditor"]', {
				resizeType : 1,
				allowPreviewEmoticons : false,
				allowImageUpload : false,
				afterBlur: function(){this.sync();},
				items : [
					'fontname', 'fontsize', '|', 'emoticons', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
					'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
					'insertunorderedlist', '|', 'image']
			});		
		});		
	</script> 
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  
	function saveEdit(){
		var modalElement=getEditHtml();
		var close=$("input[name=close]:checked").val();	
		if(close==1){
		modalElement.children().find(".close").removeClass("hide");
		}else{
		modalElement.children().find(".close").addClass("hide");
		}
		var style=document.Editform.style.value;
		modalElement.children().find(".paragraph").attr("class","paragraph"+" "+style);	
		var kongge=$("input[name=kongge]:checked").val();	
		if(kongge==1){
			modalElement.children().find(".paragraph-body").addClass("index");	
		}else{
			modalElement.children().find(".paragraph-body").removeClass("index");	
		}
		var content=document.Editform.content.value;
		modalElement.children().find(".paragraph-body").html(content);
	}	
	
</script>
</body>
</html>
