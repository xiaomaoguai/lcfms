<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">背景色：</label>
	<div class="controls">
	<select id="simple-colorpicker-1" class="hide">
		<option data-class="blue" value="#307ECC">#307ECC</option>
		<option data-class="blue2" value="#5090C1">#5090C1</option>
		<option data-class="blue3" value="#6379AA">#6379AA</option>
		<option data-class="green" value="#82AF6F">#82AF6F</option>
		<option data-class="green2" value="#2E8965">#2E8965</option>
		<option data-class="green3" value="#5FBC47">#5FBC47</option>
		<option data-class="red" value="#E2755F">#E2755F</option>
		<option data-class="red2" value="#E04141">#E04141</option>
		<option data-class="red3" value="#D15B47">#D15B47</option>
		<option data-class="orange" value="#FFC657">#FFC657</option>
		<option data-class="purple" value="#7E6EB0">#7E6EB0</option>
		<option data-class="pink" value="#CE6F9E">#CE6F9E</option>
		<option data-class="dark" value="#404040">#404040</option>
		<option data-class="grey" selected value="#848484">#848484</option>
		<option data-class="default" value="#EEE">#EEE</option>
	</select>
	</div>	
</div>
<div class="form-group">
	<label class="control-label" type="text">带标题：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="title" type="radio" class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="title" type="radio" class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">action：</label>
	<div class="controls">
		<input type="text" name="action" value="" placeholder="action" style="width:60%" />
		（&符号将被转义）
	</div>	
</div>
<div class="form-group">
	<label class="control-label" type="text">method：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="method" type="radio" class="ace" value="1" checked/>
			<span class="lbl"> post</span>
		</label>
		<label>
			<input name="method" type="radio" class="ace" value="0"/>
			<span class="lbl"> get</span>
		</label>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="addFrom();">添加表单内容</button>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  
	$(document).ready(function(){
		$('#simple-colorpicker-1').ace_colorpicker().on('change', function(){
			var modalElement=getEditHtml();
			var color_class = $(this).find('option:selected').data('class');
			if(color_class != 'default'){
			var new_class = 'form-box form-color-'+color_class;
			modalElement.children().find(".form-box").attr("class",new_class);
			modalElement.children().find(".btn").attr("class","btn btn-sm btn-"+color_class);
			}	
		});
	});
	function saveEdit(){  
		var modalElement=getEditHtml();		
		var title=$("input[name=title]:checked").val();
		if(title==1){
			modalElement.children().find(".form-header").removeClass("hide");
		}else{
			modalElement.children().find(".form-header").addClass("hide");
		}
		var action=document.Editform.action.value;
		var form=modalElement.children().find("form");	
		if(action!=""){
			form.attr("action",action);
		}
		var method=$("input[name=method]:checked").val();
		if(method==1){
			form.attr("method","post");
		}else{
			form.attr("method","get");
		}
	}
	function addFrom(){
		var edit=parent.document.getElementById("menu-fa-edit");
		$(edit).addClass("open");
		$(edit).children(".submenu").show();
		var beaker=parent.document.getElementById("menu-fa-flask");
		$(beaker).removeClass("open");	
		$(beaker).children(".submenu").hide();
	}
</script>
</body>
</html>

