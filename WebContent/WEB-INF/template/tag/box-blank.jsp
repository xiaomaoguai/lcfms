<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">中间线条：</label>
	<div class="controls">
		<input type="text" name="line" value="1px dashed"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">线条色：</label>
	<div class="controls">
	<select id="simple-colorpicker-2" name="bordercolor" class="hide">	
        <option value="#DDDDDD">#DDDDDD</option>	
		<option value="#d06b64">#d06b64</option>
		<option value="#f83a22">#f83a22</option>
		<option value="#fa573c">#fa573c</option>
		<option value="#ff7537">#ff7537</option>
		<option value="#ffad46">#ffad46</option>
		<option value="#42d692">#42d692</option>
		<option value="#16a765">#16a765</option>
		<option value="#7bd148" selected>#7bd148</option>
		<option value="#b3dc6c">#b3dc6c</option>
		<option value="#fbe983">#fbe983</option>
		<option value="#fad165">#fad165</option>
		<option value="#92e1c0">#92e1c0</option>
		<option value="#9fe1e7">#9fe1e7</option>
		<option value="#9fc6e7">#9fc6e7</option>
		<option value="#4986e7">#4986e7</option>
		<option value="#9a9cff">#9a9cff</option>
		<option value="#b99aff">#b99aff</option>
		<option value="#c2c2c2">#c2c2c2</option>
		<option value="#cabdbf">#cabdbf</option>
		<option value="#cca6ac">#cca6ac</option>
		<option value="#f691b2">#f691b2</option>
		<option value="#cd74e6">#cd74e6</option>
		<option value="#a47ae2">#a47ae2</option>
		<option value="#555">#555</option>
	</select>
	</div>	
</div>
<div class="form-group">
	<label class="control-label" type="text">高度：</label>
	<div class="controls">
		<select class="form-control input-medium" name="height">
		<option value="2">2</option>
		<option value="4">4</option>	
		<option value="6">6</option>
		<option value="8">8</option>	
		<option value="10">10</option>
		<option value="12">12</option>
		<option value="14" selected>14</option>
		<option value="16">16</option>	
		<option value="18">18</option>
		<option value="20">20</option>	
		<option value="22">22</option>
		<option value="24">24</option>	
		<option value="26">26</option>
		<option value="28">28</option>
		<option value="30">30</option>			
		<option value="30">32</option>			
		</select>	
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">
     jQuery(function($) {										
		$('#simple-colorpicker-2').ace_colorpicker();				
	});
	function saveEdit(){	
	    var modalElement=getEditHtml();
		var line=document.Editform.line.value;
		var bordercolor=document.Editform.bordercolor.value;
		var l="style=\"border-bottom:"+line+" "+bordercolor+"\"";
	    var height=document.Editform.height.value;
		var str="<div class=\"space-"+height+"\" "+l+"></div>";
		modalElement.children().find(".row").html(str);
	}	
</script>
</body>
</html>
