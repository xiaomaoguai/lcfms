<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
<link rel="stylesheet" href="${CSS}accordion.css"/>	
<script src="${JS}jquery-3.2.1.min.js"></script>
<script src="${JS}bootstrap.min.js"></script>
<script src="${JS}accordion.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12" style="padding-top: 12px;">
				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a href="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
							 <i class="fa fa-chevron-right"></i>&nbsp; 栏目1
							</a>
						</div>
						<div class="panel-collapse collapse" id="accordion1">
							<div class="panel-body">栏目1内容</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a href="#accordion2" data-toggle="collapse" class="accordion-toggle collapsed">
							 <i class="fa fa-chevron-right"></i>&nbsp; 栏目2
							</a>
						</div>
						<div class="panel-collapse collapse" id="accordion2">
							<div class="panel-body">栏目2内容</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a href="#accordion3" data-toggle="collapse" class="accordion-toggle collapsed">
							 <i class="fa fa-chevron-right"></i>&nbsp; 栏目3
							</a>
						</div>
						<div class="panel-collapse collapse" id="accordion3">
							<div class="panel-body">栏目3内容</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
$(function(){
	collapse.init();
});
</script>
</html>
