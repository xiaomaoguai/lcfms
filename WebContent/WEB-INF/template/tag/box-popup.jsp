<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<link rel="stylesheet" href="${CSS}chosen.min.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script type="text/javascript" src="${JS}chosen.jquery.min.js"></script> 
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">弹出层顶部标题：</label>
	<div class="controls">
		<div class="clearfix">
			<textarea name="toptitle" placeholder="顶部标题" cols="40" rows="5" style="text-align:left;"><h4 class='smaller'><i class='fa fa-edit'></i>弹出层</h4></textarea>			
		</div>
	支持html格式	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">宽度：</label>
	<div class="controls">
		<div class="clearfix">
			<input class="col-xs-4" type="text" name="width" placeholder="宽度" value="380"/> 
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">高度：</label>
	<div class="controls">
		<div class="clearfix">
			<input class="col-xs-4" type="text" name="height" placeholder="高度" value="200"/> 
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">支持拉伸：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="resizable" type="radio"  class="ace" value="true" /> 
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="resizable" type="radio"  class="ace" value="false" checked/> 
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">背景模态：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="modal" type="radio"  class="ace" value="true" checked /> 
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="modal" type="radio"  class="ace" value="false"/> 
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">可拖拽：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="draggable" type="radio"  class="ace" value="true" checked /> 
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="draggable" type="radio"  class="ace" value="false"/> 
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  	
	function saveEdit(){
		var modalElement=getEditHtml();
	    var toptitle=document.Editform.toptitle.value;
		var resizable=$("input[name=resizable]:checked").val();
		var modal=$("input[name=modal]:checked").val();
		var draggable=$("input[name=draggable]:checked").val();
		var width=document.Editform.width.value;
		var height=document.Editform.height.value;
		var r=randomNumber(1000,9999);
		var str="<div style=\"margin: 15px;\">"+
				"<a href=\"javascript:open("+r+");\">触发弹出层</a>"+
				"<div id=\"dialog-id-"+r+"\" class=\"hide\" tabindex=\"0\" modal=\""+modal+"\" resizable=\""+resizable+"\" draggable=\""+draggable+"\" width=\""+width+"\" height=\""+height+"\" toptitle=\""+toptitle+"\">"+
				"</div>"+
				"</div>";
		modalElement.children(".ui-dialog").remove();
		modalElement.children(".ui-widget-overlay").remove();
       	modalElement.children().find(".row").html(str);     	
	}	
</script>
</body>
</html>

